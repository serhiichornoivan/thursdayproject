package test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import java.util.*;

import java.io.File;

/**
 * Created by blizardinka on 12.04.17.
 */
public class InsertEmp {
    public static void main(String[] args) {
//        Configuration cfg = new Configuration();
//        cfg.configure(
//                new File("src/main/resources/hibernate.cfg.xml"));
//        SessionFactory sf = cfg.buildSessionFactory();
        Session s = buildSessionFactory().openSession();
        Transaction tx = s.beginTransaction();

        Employee emp = new Employee();
        emp.setId(1);
        emp.setName("Serhii");
        emp.setMobile(1);
        emp.setEmail("dot@tod");
        s.save(emp);
        s.flush();
        tx.commit();
        s.close();
//

    }
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {

            SessionFactory sessionFactory = new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addResource("Empl.hbm.xml")
                    .buildSessionFactory();

            return sessionFactory;

        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
}
